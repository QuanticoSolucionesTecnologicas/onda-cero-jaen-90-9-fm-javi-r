//
//  Onda_Cero_Jaen_90_9_FMApp.swift
//  Onda Cero Jaen 90.9 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import SwiftUI

@main
struct Onda_Cero_Jaen_90_9_FMApp: App {

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
