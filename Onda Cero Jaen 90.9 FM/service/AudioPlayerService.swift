//
//  AudioPlayerService.swift
//  Onda Cero Madrid Sur 92.7 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import AVFoundation
import MediaPlayer

class AudioPlayerService: ObservableObject {
    private var player: AVPlayer?
    private var playerItem: AVPlayerItem?
    @Published private var isPlaying = false
    
    init(url: URL) {
        playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        
        configureAudioSession()
        setupRemoteTransportControls()
        setupNowPlayingInfo(title: "90.9 FM", artist: "Onda Cero Jaén", imageName: "Icon-1024")
    }
    
    private func configureAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print("Error al configurar la sesión de audio: \(error)")
        }
    }
    
    private func setupRemoteTransportControls() {
        let commandCenter = MPRemoteCommandCenter.shared()
        
        commandCenter.playCommand.addTarget { [weak self] _ in
            self?.play()
            return .success
        }
        
        commandCenter.pauseCommand.addTarget { [weak self] _ in
            self?.pause()
            return .success
        }
    }

    // Configurar la información de reproducción en el centro de información de reproducción actual (MPNowPlayingInfoCenter)
    func setupNowPlayingInfo(title: String, artist: String, imageName: String) {
        guard let artworkImage = UIImage(named: imageName) else {
            return
        }
        
        var nowPlayingInfo = [String: Any]()
        nowPlayingInfo[MPMediaItemPropertyTitle] = title
        nowPlayingInfo[MPMediaItemPropertyArtist] = artist
        nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: artworkImage.size) { _ in
            return artworkImage
        }
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    func play() {
        guard let player = player, !isPlaying else { return }
        
        player.play()
        isPlaying = true
    }
    
    func pause() {
        guard let player = player, isPlaying else { return }
        
        player.pause()
        isPlaying = false
    }
    
    func playInBackground() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async { // Envía la actualización de la propiedad en el hilo principal
                self.play()
            }
        }
    }
    
    func pauseInBackground() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async { // Envía la actualización de la propiedad en el hilo principal
                self.pause()
            }
        }
    }
}
