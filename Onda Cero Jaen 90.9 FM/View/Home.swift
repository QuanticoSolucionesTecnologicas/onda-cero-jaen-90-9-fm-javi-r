//
//  Home.swift
//  Onda Cero Jaen 90.9 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import SwiftUI

struct Home: View {
    
    // Ocultando el tab Bar...
    init(){
        UITabBar.appearance().isHidden = true;
    }
    
    @StateObject var menuData = MenuViewModel()
    @StateObject var audioPlayerService = AudioPlayerService(url: URL(string: "https://livefastly-webs.ondacero.es/jaen-pull/audio/chunklist.m3u8")!)

    @State private var offset: CGFloat = 0
    @State private var position: CGFloat = 0
    
    var body: some View {
        
        HStack(spacing: 0) {
            // Drawer y vista principal
            // Drawer
            Drawer()
            
            // Vista principal
            TabView (selection: $menuData.selectedMenu){
                
                Radio()
                    .tag("Radio")
                    .onTapGesture {
                        menuData.showDrawer = false
                    }
                
                Contact()
                    .tag("Contáctanos")
                    .onTapGesture {
                        menuData.showDrawer = false
                    }

            }
            .frame(width: UIScreen.main.bounds.width)
            .onChange(of: menuData.selectedMenu) { newValue in
                if newValue == "Nuestra web" {
                    guard let url = URL(string: "https://www.ondacerojaen.es") else { return }
                    UIApplication.shared.open(url)
                } else if newValue == "Podcast" {
                    guard let url = URL(string: "https://www.ondacerojaen.es/podcasts") else { return }
                    UIApplication.shared.open(url)
                }
            }
        }
        
        // Max Frame
        .frame(width: UIScreen.main.bounds.width)
        // Moviendo la vista
        .offset(x: menuData.showDrawer ? 125 : -125)
        .overlay(
            ZStack{
                if !menuData.showDrawer {
                    if offset <= 10 {
                        DrawerCloseButton()
                            .padding()
                    }
                    
                } else {
                    DrawerCloseButton()
                        .opacity(0)
                }
            },
            alignment: .topLeading
        )
        .gesture(
            DragGesture()
                .onChanged { gesture in
                    position = min(gesture.translation.width, 250)
                    if menuData.showDrawer || position > 0 {
                        offset = position > 0 ? position : 0
                        menuData.showDrawer = true
                    }
                }
                .onEnded { gesture in
                    if position < 10 {
                        withAnimation(.easeInOut) {
                            menuData.showDrawer = false
                        }
                    } else {
                        withAnimation(.spring()) {
                            offset = menuData.showDrawer ? 0 : 250
                        }
                    }
                }
                
        )
        .offset(x: min(offset, 250))
        .animation(.spring())
        .environmentObject(menuData)
        .environmentObject(audioPlayerService)
        .onChange(of: menuData.selectedMenu) {
            newValue in withAnimation {
                menuData.showDrawer = false
            }
        }
        .background(Color("background-color"))
    }
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}

