//
//  Drawer.swift
//  Onda Cero Jaen 90.9 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import SwiftUI

struct Drawer: View {
    
    @EnvironmentObject var menuData: MenuViewModel
    
    var body: some View {
        
        VStack {
            
            VStack{
                
                Image("icon")
                    .resizable()
                    .frame(width: 220, height: 150)
                
                Text("Jaén 90.9 FM")
                    .foregroundColor(Color.white)
                    .font(Font.custom("Arial", size: 25))
                    .fontWeight(.bold)
                    .frame(width: 250)

                Spacer()
                    .frame(height: 15)
            }
            .padding()
            .background(Color("background-drawer")
                .ignoresSafeArea(.all, edges: .vertical))
            
            // Botones del menú
            VStack(spacing: 10){
                
                MenuButton(name: "Radio en directo", image: "radio.fill", selectedMenu: $menuData.selectedMenu)
                
                MenuButton(name: "Nuestra web", image: "globe", selectedMenu: $menuData.selectedMenu)
                
                MenuButton(name: "Podcast", image: "music.note", selectedMenu: $menuData.selectedMenu)
                
                MenuButton(name: "Contáctanos", image: "person.fill", selectedMenu: $menuData.selectedMenu)
                    
            }
            
            Spacer()
            
            VStack(spacing: 5) {
                Text("Síguenos")
                    .foregroundColor(Color.white)
                    .font(.title2)
                HStack{
                    
                    Button(action: {
                        guard let url = URL(string: "https://www.facebook.com/OndaCeroJaenOficial") else { return }
                        UIApplication.shared.open(url)
                    }) {
                        Image("facebook-blanco")
                            .padding(.horizontal, -4)
                    }
                      
                    Button(action: {
                        guard let url = URL(string: "https://twitter.com/OndaCeroJaen") else { return }
                        UIApplication.shared.open(url)
                    }) {
                        Image("twitter")
                            .padding(.horizontal, -4)
                    }
                    
                }
                Text("@2023 Quántico Soluciones Tecnológicas S.L.")
                    .foregroundColor(Color.white)
                    .font(.caption2)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 5)
            }
            
        }
        // Tamaño por defecto
        .frame(width: 250)
        .background(
            Color("background-color")
                .ignoresSafeArea(.all, edges: .vertical)
        )
    }
}

struct Drawer_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}

// Botón del drawer
struct DrawerCloseButton: View {
    
    @EnvironmentObject var menuData: MenuViewModel
    @EnvironmentObject var audioPlayerService: AudioPlayerService
    
    var body: some View {
        
        Button(action:{
            withAnimation(.easeInOut){
                menuData.showDrawer.toggle()
            }
        }, label: {
                
            VStack(spacing: 5){
                Capsule()
                    .fill(Color.white)
                    .frame(width: 35, height: 3)
                VStack(spacing: 5){
                    Capsule()
                        .fill(Color.white)
                        .frame(width: 35, height: 3)
                    Capsule()
                        .fill(Color.white)
                        .frame(width: 35, height: 3)
                }
                
            }
            
        })
    }
    
}

