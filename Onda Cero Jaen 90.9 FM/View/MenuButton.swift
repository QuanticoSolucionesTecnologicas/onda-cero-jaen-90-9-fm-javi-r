//
//  MenuButton.swift
//  Onda Cero Jaen 90.9 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import SwiftUI

struct MenuButton: View {
    var name: String
    var image: String
    @Binding var selectedMenu: String
    var body: some View {
        
        Button(action: {
            withAnimation(.spring()){
                selectedMenu = name
            }
        }, label: {
         
            HStack(spacing: 15){
                
                Image(systemName: image)
                    .font(.title2)
                    .foregroundColor(Color.white)
                
                Text(name)
                    .foregroundColor(Color.white)
            }
            .padding(.vertical, 12)
            .frame(width: 200, alignment: .leading)
            .background(Color("background-color"))
        })
        
    }
    
}

struct MenuButton_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}

