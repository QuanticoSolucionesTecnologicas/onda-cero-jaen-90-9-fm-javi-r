//
//  URLType.swift
//  Onda Cero Jaén 90.9 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import Foundation

enum URLType {
  case name(String)
  case url(URL)

  var url: URL? {
    switch self {
      case .name(let name):
        return Bundle.main.url(forResource: name, withExtension: "gif")
      case .url(let remoteURL):
        return remoteURL
    }
  }
}
