//
//  Contact.swift
//  Onda Cero Jaen 90.9 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import SwiftUI

struct Contact: View {
    var body: some View {
        NavigationView{
            ScrollView(){
                
                Image("ondacero_blanco_horizontal")
                    .resizable()
                    .scaledToFit()
                    .frame(width: UIScreen.main.bounds.width, height: 130)
                    .padding(.top, 30)
                
                Text("Onda Cero Jaén")
                    .font(.title)
                    .foregroundColor(.white)
                
                Spacer()
                
                Text("Escucha en directo toda la actualidad y últimas noticias en nuestros programas. Una apuesta por la participación de los oyentes. Una radio abierta a toda la sociedad, por eso nuestra seña de identidad es clara, dar protagonismo a toda la comunidad.")
                    .foregroundColor(.white)
                    .frame(width: UIScreen.main.bounds.width * 0.9)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, UIScreen.main.bounds.width * 0.1)
                    .font(.system(size: 19))
                
                Spacer()
                
                HStack{
                    Text("Email:")
                        .frame(width: UIScreen.main.bounds.width * 0.32)
                    Text("Teléfono:")
                        .frame(width: UIScreen.main.bounds.width * 0.32)
                    Text("Visita la web:")
                        .frame(width: UIScreen.main.bounds.width * 0.36)
                        .padding(.trailing, 2)
                }
                
                .padding(.top, 20)
                .foregroundColor(.white)
                .font(.title3)
                
                Spacer()
                
                HStack{
                    
                    Button(action: {
                        if let emailURL = URL(string: "mailto:ondacerojaen@ondacerojaen.es") {
                           UIApplication.shared.open(emailURL, options: [:], completionHandler: nil)
                        }
                    }) {
                        Image(systemName: "envelope.fill")
                            .foregroundColor(.white)
                            .font(.system(size: 50))
                    }
                    .frame(width: UIScreen.main.bounds.width * 0.32)

                    Button(action: {
                        if let phoneCallURL = URL(string: "tel://953276300") {
                            let application:UIApplication = UIApplication.shared
                            if (application.canOpenURL(phoneCallURL)) {
                                application.open(phoneCallURL, options: [:], completionHandler: nil)
                            }
                        }

                    }) {
                        Image(systemName: "phone.fill")
                            .foregroundColor(.white)
                            .font(.system(size: 50))
                    }
                    .frame(width: UIScreen.main.bounds.width * 0.32)
                    
                    
                    Button(action: {
                        guard let url = URL(string: "https://www.ondacerojaen.es") else { return }
                        UIApplication.shared.open(url)
                    }) {
                        Image(systemName: "globe")
                            .foregroundColor(.white)
                            .font(.system(size: 50))
                    }
                    .frame(width: UIScreen.main.bounds.width * 0.36)
                }
                
                
                VStack{
                    Text("Aplicación desarrollada por:")
                        .padding(.top, 20)
                        .foregroundColor(.white)
                        .font(.system(size: 17))
                        
                    Button(action: {
                        guard let url = URL(string: "https://www.quanticoweb.com") else { return }
                        UIApplication.shared.open(url)
                    }) {
                        Image("Logotipo Quantico-01")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 250, height: 90)
                    }

                }
                
            }
            .background(Color("background-color")
                .ignoresSafeArea(.all, edges: .vertical)
            )
            .navigationBarItems(trailing:
                AnyView(
                    Text("Contáctanos")
                        .font(.title2)
                        .foregroundColor(.white)
                        .bold()
                        .frame(width: UIScreen.main.bounds.width, height: 60)
                        .background(Color("background-drawer"))
                        .padding(.top, UIScreen.main.bounds.height * 0.02)
                )
            )
        }
    }
}

struct Contact_Previews: PreviewProvider {
    static var previews: some View {
        Contact()
    }
}

