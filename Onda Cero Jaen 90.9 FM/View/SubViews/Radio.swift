//
//  Radio.swift
//  Onda Cero Jaen 90.9 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import SwiftUI

struct Radio: View {
    @State private var isPlaying = false
    @State private var showProgressBar = false
    @State private var buttonOpacity: Double = 1.0
    @EnvironmentObject var audioPlayerService: AudioPlayerService
    
    var body: some View {
        NavigationView{
            VStack{
                
                GalleryViewController()
                    .frame(height: UIScreen.main.bounds.height * 0.45)
                    .padding(.horizontal, UIScreen.main.bounds.width * 0.075)
                    
                Spacer()
                    .frame(height: UIScreen.main.bounds.width * 0.09)
                
                if !isPlaying {
                    Text("Click en el botón de Play para reproducir")
                        .frame(width: UIScreen.main.bounds.width * 0.90)
                        .foregroundColor(.white)
                } else {
                    Text("Click en el botón de Pause para pausar")
                        .frame(width: UIScreen.main.bounds.width * 0.90)
                        .foregroundColor(.white)
                }
                if !isPlaying{
                    Button(action: {
                        showProgressBar = true
                    }) {
                        Image("play")
                            .resizable()
                            .frame(width: 120, height: 120)
                    }
                    .opacity(buttonOpacity)
                    
                } else {
                    Button(action: {
                        isPlaying.toggle()
                        audioPlayerService.pauseInBackground()
                    }) {
                        Image("pause")
                            .resizable()
                            .frame(width: 120, height: 120)
                    }
                }
                
                if showProgressBar {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: .white))
                        .scaleEffect(3.75)
                        .padding(.top, -80)
                        .onAppear(perform: {
                            buttonOpacity = 0.0
                            audioPlayerService.playInBackground()
                            startProgressBarTimer()
                        })
                }
                
                Spacer()
                    .frame(height: UIScreen.main.bounds.width * 0.09)
                
                HStack {
                    VStack {
                        if !isPlaying {
                            Rectangle()
                                .foregroundColor(Color.white)
                                .frame(width: UIScreen.main.bounds.width * 0.4, height: 2)
                                .padding(.horizontal, 15)
                        } else {
                            GIFView(type: .name("vumetro"))
                                .frame(width: UIScreen.main.bounds.width * 0.47, height: 90)
                                
                        }
                    }
                    Image("online")
                    
                    if !isPlaying {
                        Text("En Espera")
                            .foregroundColor(.white)
                            .padding(.trailing, 15)
                    } else {
                        Text("En Directo")
                            .foregroundColor(.white)
                            .padding(.trailing, 15)
                    }
                }
                .frame(height: UIScreen.main.bounds.width * 0.2)
                .background(Color("background-cardview"))
                .padding(.bottom, 0)
                .cornerRadius(10)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.green, lineWidth: 2)
                )
                
                
            }
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            .background(Color("background-color")
                .ignoresSafeArea(.all, edges: .vertical)
            )
            .navigationBarItems(trailing:
                AnyView(
                    Image("ondacero_blanco_horizontal")
                        .resizable()
                        .scaledToFit()
                        .frame(width: UIScreen.main.bounds.width, height: 60)
                        .background(Color("background-drawer"))
                        .padding(.top, UIScreen.main.bounds.height * 0.02)
                )
            )
        }
        
    }
    
    func startProgressBarTimer() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            showProgressBar = false
            performAction()
            buttonOpacity = 1.0
        }
    }
        
    func performAction() {
        isPlaying.toggle()
    }
    
}

struct Radio_Previews: PreviewProvider {
    static var previews: some View {
        Radio()
    }
}

class ImageCollectionViewCell: UICollectionViewCell {
    var imageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)

        // Configuración de la vista de imagen
        imageView = UIImageView(frame: contentView.bounds)
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        contentView.addSubview(imageView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

struct GalleryViewController: UIViewControllerRepresentable {
    typealias UIViewControllerType = UIViewController

    func makeUIViewController(context: Context) -> UIViewController {
        let viewController = UIViewController()

        // Configuración de la vista de colección
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.85, height: UIScreen.main.bounds.height * 0.45)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.sectionInset = .zero

        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 0.85, height: UIScreen.main.bounds.height * 0.45), collectionViewLayout: layout)
        collectionView.dataSource = context.coordinator
        collectionView.delegate = context.coordinator
        collectionView.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: "ImageCell")
        collectionView.isScrollEnabled = false
        viewController.view.addSubview(collectionView)

        // Iniciar la animación para pasar las imágenes automáticamente cada 5 segundos
        let timer = Timer.scheduledTimer(timeInterval: 5.0, target: context.coordinator, selector: #selector(Coordinator.moveToNextImage), userInfo: nil, repeats: true)
        context.coordinator.timer = timer

        return viewController
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        // Actualización de la vista de colección si es necesario
    }

    func makeCoordinator() -> Coordinator {
        Coordinator()
    }

    class Coordinator: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
        var collectionView: UICollectionView?
        let imageNames = ["banner0", "banner1", "banner2"]
        let imageLinks = ["https://www.quanticoweb.com", "https://www.alquilerseguro.es", "https://www.quanticoweb.com/desarrollo-software-jaen"]
        var currentImageIndex = 0
        var timer: Timer?

        @objc func moveToNextImage() {
            guard let collectionView = collectionView else { return }
            let nextIndex = (currentImageIndex + 1) % imageNames.count
            let indexPath = IndexPath(item: nextIndex, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            currentImageIndex = nextIndex
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }

        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return imageNames.count // Devuelve el número de imágenes en el conjunto de datos
        }

        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCollectionViewCell
            let imageName = imageNames[indexPath.item] // Obtén el nombre de la imagen correspondiente al índice
            cell.imageView.image = UIImage(named: imageName)

            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            // Agregar el tapGestureRecognizer a la vista de imagen
            cell.imageView.addGestureRecognizer(tapGestureRecognizer)

            self.collectionView = collectionView // Asignar la instancia de la colección de vista a la propiedad collectionView

            return cell
        }

        @objc func imageTapped(sender: UITapGestureRecognizer) {
            let link = imageLinks[currentImageIndex]

            if let url = URL(string: link) {
                UIApplication.shared.open(url)
            }
        }

        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            // Acción al seleccionar una celda
        }
    }
}

