//
//  MenuModel.swift
//  Onda Cero Madrid Sur 92.7 FM
//
//  Created by Usuario desarrollador on 9/5/23.
//

import SwiftUI

// Menu Data...

class MenuViewModel: ObservableObject{
    
    // Vista por defecto
    @Published var selectedMenu = "Radio en directo"
    
    // Mostrar...
    @Published var showDrawer = false
}
