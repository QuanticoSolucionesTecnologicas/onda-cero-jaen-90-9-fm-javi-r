# 2023-Ondacero-Jaén
Aplicación desarrollada para Onda Cero Jaén con el fin de la reproducción de audio en segundo plano de la emisora. Dicha reproducción de audio tiene una notificación asociada con la cual se puede pausar y reanudar el audio.

En el fragmento principal se disponen una serie de banners con un enlace asociado que no consumen de una API, es decir, los enlaces y las imagenes se controlan desde el propio proyecto. La aplicación cuenta con un drawer con el que se puede acceder a sus redes sociales, página web y a otro fragmento de contacto.

## Dependencias
La aplicación cuenta con una única dependencia añadida de manera manual que es FLAnimatedImage, utilizada para la reproducción del gif del sonido. FLAnimatedImage es un componente directo bien encapsulado. Simplemente trata de reemplazar las instancias de `UIImageView` con instancias de `FLAnimatedImageView` para obtener compatibilidad con GIF animados. No dispone de caché central o estado para administrar. Esta es la clase asociada para ello:

```swift
//
//  GifView.swift
//  Onda Cero Jaén 90.9 FM
//
//  Created by Usuario desarrollador on 29/5/23.
//

import SwiftUI
import FLAnimatedImage

struct GIFView: UIViewRepresentable {
  private var type: URLType

  init(type: URLType) {
    self.type = type
  }

  private let imageView: FLAnimatedImageView = {
    let imageView = FLAnimatedImageView()
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.layer.cornerRadius = 24
    imageView.layer.masksToBounds = true
    return imageView
  }()

  private let activityIndicator: UIActivityIndicatorView = {
    let activityIndicator = UIActivityIndicatorView()
    activityIndicator.translatesAutoresizingMaskIntoConstraints = false
    return activityIndicator
  }()
}

extension GIFView {
  func makeUIView(context: Context) -> UIView {
    let view = UIView(frame: .zero)

    view.addSubview(activityIndicator)
    view.addSubview(imageView)

    imageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    imageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true

    activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

    return view
  }

  func updateUIView(_ uiView: UIView, context: Context) {
    activityIndicator.startAnimating()
    guard let url = type.url else { return }

    DispatchQueue.global().async {
      if let data = try? Data(contentsOf: url) {
        let image = FLAnimatedImage(animatedGIFData: data)

        DispatchQueue.main.async {
          activityIndicator.stopAnimating()
          imageView.animatedImage = image
        }
      }
    }
  }
}

```

## Sistema operativo mínimo
La aplicación está diseñada para que la versión de android mínima necesaria para poder instalarse sea la versión 14.0 ya que dicha versión es la mínima necesaria para el funcionamiento de algunas partes del código de la aplicación.

## Permisos de la aplicación
La aplicación cuenta con la necesidad de un permiso del cual no es necesaria la confirmación del usuario que va a interactuar con la aplicación para su funcionamiento.
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>UIApplicationSceneManifest</key>
	<dict>
		<key>UISceneConfigurations</key>
		<dict/>
	</dict>
	<key>UIBackgroundModes</key>
	<array>
		<string>audio</string>
	</array>
</dict>
</plist>

```

